# Rolling Average Video Classification

doing video classification on short sports clips using a rolling average from simple predictions on single video frames.


## Example

Rolling average prediction on a [soccer game](https://gitlab.com/maternusherold/rolling-avg-video-classification/-/blob/master/classification_output/fifa_soccer_compressed.mp4). While the prediction (top left corner) becomes stable after some seconds, the shortcomings are visible in the beginning, where the prediction is undecided and picking a proper length for the rolling average is crucial to ensure stable predictions. 

<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="./classification_output/fifa_soccer_compressed.mp4" type="video/mp4">
  </video>
</figure>
<!-- blank line -->

## TODOs

- cold start of model
- model accuracy 
