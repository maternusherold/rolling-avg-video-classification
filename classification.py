"""Using a rolling average on single frame classifications.

Instead of training a complicated recurrent network or transformer the
classification is based on a rolling average carried out on predictions of single
frames of the video.
"""

import cv2
import numpy as np
import pickle
import torch
from collections import deque
from utils.utils import print_info
from model.model import SportsModel


class VideoClassifier:

    def __init__(self, model: torch.nn.Module, path_to_video: str, queue_size: int,
                 path_encoding: str, output_path: str, frame_size: tuple = (224, 224),
                 draw_frames: bool = False):
        """Classifies activity in video based on rolling average on predictions.

        :param model: model for classification
        :param path_to_video: path to video file
        :param queue_size: length of rolling avg. queue
        :param path_encoding: path to encoding
        :param output_path: path to write outputs
        :param frame_size: input frame size
        :param draw_frames: to either draw frames
        """
        self.model = model
        self.video = cv2.VideoCapture(path_to_video)
        self.queue = deque(maxlen=queue_size)
        self.encoding, self.decoding = self.revert_encoding(path_encoding)
        self.path_output = output_path
        self.frame_size = frame_size
        self.video_writer = cv2.VideoWriter_fourcc(*'MJPG')
        self.draw_frames = draw_frames
        self.writer = None
        self.frame_width = None
        self.frame_height = None

    @staticmethod
    def revert_encoding(path_to_enc: str):
        """Revert encoding for easier look up. """
        file = pickle.load(open(path_to_enc, "rb"))
        ret = {}
        for key, value in file.items():
            sport_index = np.argmax(value, axis=0)
            ret[sport_index] = key
            file[key] = sport_index
        return file, ret

    @staticmethod
    def draw_prediction_on_frame(frame, pred):
        """Write prediction on the frame. """
        text = f"activity: {pred}"
        return cv2.putText(frame, text, (35, 50), cv2.FONT_HERSHEY_SIMPLEX,
                           1.25, (0, 255, 0), 0)

    @staticmethod
    def show_frame(frame):
        """Show frame incl. prediction. """
        cv2.imshow("Prediction", frame)
        return cv2.waitKey(1) & 0xFF

    def write_video(self, frame):
        """Write video to disk. """
        # init writer once in the beginning
        if self.writer is None:
            self.writer = cv2.VideoWriter(self.path_output, self.video_writer,
                                          30, (self.frame_width,
                                               self.frame_height), True)
        self.writer.write(frame)

    def prepare_frame(self, frame):
        """Copy frame and convert to RGB. """
        frame_copy = frame.copy()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        return cv2.resize(frame, self.frame_size).astype("float32"), frame_copy

    def predict_on_frame(self, frame):
        """Perform single prediction on video frame. """
        frame = np.transpose(frame, (2, 0, 1))
        frame = np.expand_dims(frame, axis=0)
        frame = torch.tensor(frame, dtype=torch.double)
        pred = self.model(frame)  # keep all logits
        self.queue.append(pred.detach().numpy())
        return pred

    def sliding_window_avg(self):
        """Compute argmax. over current predictions. """
        avg_pred = np.array(self.queue).mean(axis=0)
        sport_index = np.argmax(avg_pred)
        return self.decoding[sport_index]

    def compute_rolling_average(self):
        """Predict on every video frame and compute a running average. """
        print_info("starting video classification!")
        while True:
            pointer, frame = self.video.read()

            # end of video if no pointer left
            if not pointer:
                break

            # init frame sizes
            if self.frame_width is None or self.frame_height is None:
                self.frame_height, self.frame_width = frame.shape[:2]

            # prepare frame for prediction
            frame, frame_copy = self.prepare_frame(frame)

            # average over past predictions - i.e. rolling avg.
            self.predict_on_frame(frame)
            predicted_sport = self.sliding_window_avg()

            frame_drawn_pred = self.draw_prediction_on_frame(frame_copy,
                                                             predicted_sport)
            self.write_video(frame_drawn_pred)

            if self.draw_frames:
                # display `prediction
                key = self.show_frame(frame_drawn_pred)
                # quit classification loop
                if key == 'q':
                    break

        print_info("done classifying")
        print_info("cleaning up")
        self.writer.release()
        self.video_writer.release()
        print_info("DONE!")


if __name__ == "__main__":
    sports_model = SportsModel()
    sports_model.model.load_state_dict(torch.load("/home/maternus/Coding/"
                                                  "rolling_avg_pytorch/model/"
                                                  "model_output/model_20102020.pth"))
    vc = VideoClassifier(model=sports_model.model, path_to_video="./data/soccer_video.mp4",
                         queue_size=32, path_encoding="./data/encoding_21_58.p",
                         output_path="./classification_output/fifa_soccer.mp4")
    vc.compute_rolling_average()
