"""Dataset loader. """

import numpy as np
import os
import pandas as pd
import pickle
from datetime import datetime
from PIL import Image
from sklearn.preprocessing import OneHotEncoder
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms, utils


class SportsDataset(Dataset):

    def __init__(self, root_dir: str, samples_name: str, transform: transforms = None):
        """Creating a Dataset from sports images.

        :param root_dir: root directory of sports folders
        :param samples_name: file name for listed sampled
        :param transform: transformations to apply to the data
        """
        self.root = root_dir
        self.samples_list = pd.read_csv(os.path.join(self.root, samples_name))
        self.labels_list = self.get_labels()
        self.labels_encoded = self.encode_labels()
        self.transform = transform

    def __len__(self) -> int:
        """Returns total of samples in the dataset. """
        return len(self.samples_list)

    def __getitem__(self, item) -> dict:
        """Indexing of Dataset.

        :param item: index of sample to retrieve
        :returns: dict of {"label": label, "image": image}
        """
        # get label at index and then look up the encoding
        label = self.labels_encoded[self.samples_list["label"][item]]
        image_path = self.samples_list["name"][item]
        image = self.load_image(image_path)
        sample = {"label": label, "image": image}
        # apply transformations if applicable
        if self.transform:
            sample = self.transform(sample)
        return sample

    def get_labels(self):
        """Searches the root directory for all labels. """
        return [x for x in os.listdir(self.root)
                if os.path.isdir(os.path.join(self.root, x))]

    def load_image(self, image_path) -> Image:
        """Loads the image from path.

        :param image_path: relative path to the image from the root directory
        :returns: image as numpy array
        """
        image_path = os.path.join(self.root, image_path)
        return np.array(Image.open(image_path))

    def encode_labels(self):
        """Computes a one-hot encoding of the provided labels.

        :returns: dict of label encodings for look up
        """
        enc = OneHotEncoder()
        labels = np.array(self.labels_list).reshape(-1, 1)
        labels_encoded = enc.fit_transform(labels).toarray()
        ret = {}
        for i, label in enumerate(self.labels_list):
            ret[label] = labels_encoded[i, :]
        self.write_encoding(ret)
        return ret

    @staticmethod
    def write_encoding(encoding):
        """Write encoding to disk.

        :param encoding: label encoding
        """
        time = datetime.now().strftime("%H_%M")
        with open(f"./encoding_{time}.p", "wb") as file:
            pickle.dump(encoding, file)
