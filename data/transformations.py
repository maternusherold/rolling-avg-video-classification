"""Transformation classes.

As the data samples are of different sizes they need to be set to a standardized
size as well as transformations should be applied for a more robust training.

Transformations are provided as classes utilizing a call method to be stacked
as PyTorch transformations.
"""

import numpy as np
from skimage import transform


class RandomCrop:
    def __init__(self, output_size):
        """Crops an image to the desired size.

        :param output_size: either int or tuple (W,H) of desired size
        """
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            # use the single size for both dimensions
            self.output_size = (output_size, output_size)
        else:
            # make sure the tuple holds ints
            assert len(output_size) == 2
            assert isinstance(output_size[0], int) \
                   and isinstance(output_size[1], int)
            self.output_size = output_size

    def __call__(self, sample):
        """Crops a sample to the desired size.

        :param sample: data sample of shape {"label": label, "image": image}
        """
        height_now, width_now, _ = sample["image"].shape
        width_new, height_new = self.output_size
        # pick starting column and row at random
        left = np.random.randint(0, width_now - width_new)
        top = np.random.randint(0, height_now - height_new)
        # slice image accordingly
        sample["image"] = sample["image"][top:top+height_new, left:left+width_new]
        return sample


class Rescale:

    def __init__(self, output_size):
        """Rescaling the image to the desired size.

        If a tuple is provided the output size is matched directly. If an int
        is provided the aspect ratio is kept by matching the shorter edge with
        the new size.

        :param output_size: either int or (int, int)
        """
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        """Rescales the image of a sample.
        :param sample: data sample of shape {"label": label, "image": image}
        """
        height_now, width_now, _ = sample["image"].shape
        if isinstance(self.output_size, int):
            # compute size keeping aspect ratio
            if height_now > width_now:
                height_new, width_new = height_now * self.output_size / width_now, width_now
            else:
                height_new, width_new = height_now, width_now * self.output_size / height_now
        else:
            height_new, width_new = self.output_size, self.output_size

        sample["image"] = transform.resize(sample["image"],
                                           (int(height_new), int(width_new)))
        return sample


class ToTensor:
    """Converts numpy image to PyTorch tensors.

    As numpy has the images as HxWxC and PyTorch handles images as CxHxW the
    channel ordering has to be changed.
    """

    def __call__(self, sample):
        """Rearrange image channels.

        :param sample: data sample of shape {"label": label, "image": image}
        :returns: sample with PyTorch friendly channel ordering
        """
        sample["image"] = sample["image"].transpose((2, 0, 1))
        return sample
