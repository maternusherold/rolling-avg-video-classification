"""Utilizing a pretrained ResNet18 and changing the last layer. """


import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models


class SportsModel(pl.LightningModule):

    def __init__(self, base_model: models = models.resnet18(pretrained=True),
                 classes: int = 23, fine_tuning: bool = False):
        """Facilitates a pretrained ResNet adapted to sports data.

        :param base_model: pretrained model to utilize
        :param classes: different sports types to classify
        :param fine_tuning: in case of fine tuning the base model, i.e. updating all parameters
        """
        super().__init__()
        self.classes = classes
        self.fine_tune = fine_tuning
        self.model = self.prepare_model(base_model)

    def prepare_model(self, base_model):
        """Stacks model with final linear layer.

        :param base_model: pretrained model to utilize
        """
        linear_features = base_model.fc.in_features
        if not self.fine_tune:
            for param in base_model.parameters():
                param.requires_grad = False
        # require grad is on by default for new layers
        base_model.fc = nn.Linear(linear_features, self.classes)
        base_model = base_model.double()  # using double precision parameters
        return base_model

    def forward(self, x):
        """Forward pass through the model at inference.

        :param x: input data to do inference on
        :returns: predictions on input data
        """
        return self.model(x)

    def training_step(self, batch, batch_idx):
        """Complete training step.

        :param batch: batch to pass through the model
        :param batch_idx: batch index in the dataset
        :returns: loss on batch
        """
        x, y = batch["image"], batch["label"]
        y = y.max(axis=1).indices  # class indices
        # predict on sample
        y_hat = self.model(x)
        # evaluate loss
        loss = F.cross_entropy(y_hat, y)
        self.log("train_loss", loss)  # log with Tensorboard by default
        return loss

    def validation_step(self, batch, batch_idx):
        """TODO """
        x, y = batch["image"], batch["label"]
        y = y.max(axis=1).indices  # class indices
        y_hat = self(x)
        return {'val_loss': F.cross_entropy(y_hat, y)}

    def validation_epoch_end(self, outputs):
        """TODO """
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        self.log("val_loss", avg_loss)
        return avg_loss

    def configure_optimizers(self):
        """Configure optimizer used. """
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer
