"""Utility functions to establish a clean dataset.

The data at hand is in different formats, sizes and dimensions. For a simpler
pipeline the format should be brought down to a common divisor.
"""

import os
import numpy as np
import pandas as pd
from PIL import Image


def delete_too_small_images(path: str, min_size: tuple = (256, 256)):
    """Runs over the dataset and deletes too small images.

    :param path: path to top level dir
    :param min_size: min. size as WxH
    """
    df = pd.read_csv(os.path.join(path, "cleaned.csv"))
    counter_removed = 0
    for file in df["name"]:
        path_file = os.path.join(path, file)
        img = np.array(Image.open(path_file))
        if img.shape[0] < min_size[0] or img.shape[1] < min_size[1]:
            os.remove(path_file)
            counter_removed += 1
    # update files table
    check_files_table(path)
    print(f"removed {counter_removed} files in total")


def delete_one_channel_images(path: str):
    """Deletes gray-scale images.

    :param path: path to top level dir
    """
    df = pd.read_csv(os.path.join(path, "cleaned.csv"))
    counter_removed = 0
    for file in df["name"]:
        path_file = os.path.join(path, file)
        img = np.array(Image.open(path_file))
        if img.shape.__len__() < 3:
            os.remove(path_file)
            counter_removed += 1
    # update files table
    check_files_table(path)
    print(f"removed {counter_removed} files in total")


def check_files_table(path: str):
    """Checks if the file holding all file paths is still correct.

    :param path: path to top level dir
    """
    df = pd.read_csv(os.path.join(path, "cleaned.csv"))
    rows_to_drop = []
    for index, row in df.iterrows():
        if not os.path.exists(os.path.join(path, row["name"])):
            rows_to_drop.append(index)
    df.drop(rows_to_drop, inplace=True)
    df.to_csv(os.path.join(path, "cleaned.csv"))
    print(f"removed {rows_to_drop.__len__()} rows in total")
