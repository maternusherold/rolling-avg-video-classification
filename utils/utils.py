"""General utility functions. """


def print_info(info: str):
    """Prints provided info in a special format.

    :param info: information to print as string
    """
    print(f'[INFO] '.ljust(80, '='))
    print(info)
    print(''.ljust(80, '='))
